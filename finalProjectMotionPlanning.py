# Python 2/3 compatibility imports

from __future__ import print_function
from six.moves import input
from demo_ee import *
from object_control import *
from gazebo_msgs.srv import SpawnModel, SetModelConfiguration, SetModelConfigurationRequest, DeleteModel
from std_srvs.srv import Empty
from geometry_msgs.msg import Pose, Point, Quaternion
from attacher import AttachDetachHelper
from demo_ee import MovableRobot, all_close
import sys
import copy
import time
import rospy
import random
import numpy as np
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from typing import List, Callable
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt
    tau = 2.0 * pi
    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list






# THis function that I wrote moves the robot to a safe position to begin my movements
def robotToSafePosition(move_group):

    joint_goal = move_group.get_current_joint_values()
    # safe joint values
    joint_goal[0] = 0
    joint_goal[1] = -1.5707963267948966
    joint_goal[2] = -1.5707963267948966
    joint_goal[3] = -1.5707963267948966
    joint_goal[4] = 0
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)
    move_group.stop()
    time.sleep(10)

   

# This function moves my robot to the start point
def robotToBJStartPoint(move_group):
    print("Beginning to move robot to start point")
    # Clears waypoints
    waypoints = []
    x = 0
    y = 0.5
    z = 0.5
    pose = move_group.get_current_pose().pose
    pose.position.x = x
    pose.position.y = y
    pose.position.z = z + 0.75
    pose.orientation.x = 0  
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1.0
    waypoints.append(pose)

    # Computes cartesian path to starting point of my C, and executes the plan
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(5)
    print("Finished moving robot to start point")



# This function makes my robot do the stand motion
def standMotion(move_group):
    print("Beginning to Stand")

    waypoints = []  
    x_positions = [-0.5, 0, 0.5, 0.0, -0.5, 0.0, 0.5, 0]
    y_positions = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
    z_positions = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
    
    # Creating a cartesian path for each (x,y) coordinate pair above
    for i in range(len(x_positions)):
        x = x_positions[i]
        y = y_positions[i]
        z = z_positions[i]
        pose = move_group.get_current_pose().pose
        pose.position.x = x
        pose.position.y = y
        pose.position.z = z + 0.75
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1.0
        waypoints.append(pose)
    
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(5)
    print("Finished standing")
            


# This function makes my robot do the stand motion
def hitMotion(move_group):
    print("Beginning to Hit")

    waypoints = []  
    x_positions = [0, 0, 0, 0, 0]
    y_positions = [0.5, 0.7, 0.5, 0.7, 0.5]
    z_positions = [0.5, 0.3, 0.5, 0.3, 0.5]
    
    # Creating a cartesian path for each (x,y) coordinate pair above
    for i in range(len(x_positions)):
        x = x_positions[i]
        y = y_positions[i]
        z = z_positions[i]
        pose = move_group.get_current_pose().pose
        pose.position.x = x
        pose.position.y = y
        pose.position.z = z + 0.75
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1.0
        waypoints.append(pose)
    
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(5)
    print("Finished hitting")


# This function makes my robot place chips into the center
def moveChips(move_group):
    print("Beginning to move chips to bet")

    waypoints = []  
    x_positions = [0,       0.5,    0.7]
    y_positions = [0.5,     0,      0]
    z_positions = [0.5,     0.5,    0.6]
    
    # Creating a cartesian path for each (x,y) coordinate pair above
    for i in range(len(x_positions)):
        x = x_positions[i]
        y = y_positions[i]
        z = z_positions[i]
        pose = move_group.get_current_pose().pose
        pose.position.x = x
        pose.position.y = y
        pose.position.z = z + 0.75
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1.0
        waypoints.append(pose)
    
    # Computes cartesian path to starting point of my C, and executes the plan
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(5)
    #print("Finished moving chips to bet")


def rotateRobot(move_group, a, b, c, d, e, f):
    joint_goal = move_group.get_current_joint_values()
    # safe joint values
    #print("Attemtping to move wrist")
    joint_goal[0] = -2.9499816992444847 + a
    joint_goal[1] = -2.056749106390937 + b
    joint_goal[2] = -1.1951077067997113 + c
    joint_goal[3] = -1.4737444034273173 + d
    joint_goal[4] = -1.5712881472955722 + e
    joint_goal[5] = 4.520788113041066 + f

    move_group.go(joint_goal, wait=True)
    # Calling ``stop()`` ensures that there is no residual movement
    move_group.stop()
    #print("Done moving the wrist")
    time.sleep(15)

# Let's give the info of the card's suits, ranks and values

suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
values = {'Two':2, 'Three':3, 'Four':4, 'Five':5, 'Six':6, 'Seven':7, 'Eight':8, 'Nine':9, 'Ten':10, 'Jack':10,
          'Queen':10, 'King':10, 'Ace':11}


# Following below is the Card Class which will initiate a card with the given suit and rank

class Card:

    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return self.rank + ' of ' + self.suit


# Following below is the Deck Class which will create a deck from the given cards

class Deck:

    def __init__(self):
        self.deck = []  # start with an empty list
        for suit in suits:
            for rank in ranks:
                self.deck.append(Card(suit,rank))  # build Card objects and add them to the list

    def __str__(self):
        deck_comp = ''  # start with an empty string
        for card in self.deck:
            deck_comp += '\n '+card.__str__() # add each Card object's print string
        return 'The deck has:' + deck_comp

    def shuffle(self):          # shuffle function will shuffle the whole deck
        random.shuffle(self.deck)

    def deal(self):             # deal function will take one card from the deck
        single_card = self.deck.pop()
        return single_card


# Following is the Hand Class which will add the cards from deck class to the player's hand

class Hand:
    def __init__(self):
        self.cards = []  # start with an empty list as we did in the Deck class
        self.value = 0   # start with zero value
        self.aces = 0    # add an attribute to keep track of aces

# add_card function will add a card to the player's hand

    def add_card(self,card):
        self.cards.append(card)
        self.value += values[card.rank]

# since ace can have two values as 1 or 11, adjust_for_ace will adjust the value of ace


    def adjust_for_ace(self):
        while self.value > 21 and self.aces:
            self.value -= 10
            self.aces -= 1

'''     In addition to decks of cards and hands, we need to keep track of a Player's starting chips, bets, and ongoing winnings. 
This could be done using global variables, but in the spirit of object oriented programming, let's make a Chips class instead!       '''

class Chips:

    def __init__(self):
        self.total = 100  # This can be set to a default value or supplied by a user input
        self.bet = 0

    def win_bet(self):
        self.total += self.bet

    def lose_bet(self):
        self.total -= self.bet



# FUNCTION FOR TAKING BETS
def take_bet(chips):

    while True:
        try:
            chips.bet = int(input('How many chips would you like to bet? '))
        except ValueError:
            print('Sorry, a bet must be an integer!')
        else:
            if chips.bet > chips.total:
                print("Sorry, your bet can't exceed",chips.total)
            else:
                break

# function for taking hits

def hit(deck,hand):

    hand.add_card(deck.deal())
    hand.adjust_for_ace()


# function prompting the Player to Hit or Stand

def hit_or_stand(deck,hand, move_group):
    # global playing  # to control an upcoming while loop
    playing = True

    while True:
        x = input("Would you like to Hit or Stand? Enter 'h' or 's' ")

        if x[0].lower() == 'h':
            hitMotion(move_group)
            hit(deck,hand)  # hit() function defined above

        elif x[0].lower() == 's':
            standMotion(move_group)
            print("Player stands. Dealer is playing.")
            playing = False

        else:
            print("Sorry, please try again.")
            continue
        time.sleep(1)
        break
    return playing
        
# functions to display cards

def show_some(player,dealer):
    print("\nDealer's Hand:")
    print(" <card hidden>")
    print('',dealer.cards[1])
    print("\nPlayer's Hand:", *player.cards, sep='\n ')

def show_all(player,dealer):
    print("\nDealer's Hand:", *dealer.cards, sep='\n ')
    print("Dealer's Hand =",dealer.value)
    print("\nPlayer's Hand:", *player.cards, sep='\n ')
    print("Player's Hand =",player.value)


# functions to handle end of game scenarios

def player_busts(player,dealer,chips):
    print("Player busts!")
    chips.lose_bet()

def player_wins(player,dealer,chips):
    print("Player wins!")
    chips.win_bet()

def dealer_busts(player,dealer,chips):
    print("Dealer busts!")
    chips.win_bet()

def dealer_wins(player,dealer,chips):
    print("Dealer wins!")
    chips.lose_bet()

def push(player,dealer):
    print("Dealer and Player tie! It's a push.")



def spawnOurChip():
    #rospy.init_node('spawn_box')
    rospy.wait_for_service('/gazebo/spawn_urdf_model')
    spawn_model = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)

    with open("/home/cer53/catkin_ws/src/ur5e_robotiq2f140_demo/object.urdf", "r") as file:
        urdf_data = file.read()

    pose = Pose(Point(0.58, -0.02, 0.9), Quaternion(0, 0, 0, 1)) # Adjust orientation if needed
    chip_name = "chip_link"
    spawn_model(chip_name, urdf_data, "", pose, "world")
    # y was at 0.04
    # x was at 0.48



def deleteOurChip():
    rospy.wait_for_service('/gazebo/delete_model')
    delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)

    try:
        resp = delete_model("chip_link")
        return resp.success
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)





def robotBetsChips(move_group, ee_group, attach_detach_helper, chip_name):
    
    moveChips(move_group)
    rotateRobot(move_group, 0, 0, 0, 0, pi/2, 0)
    rotateRobot(move_group, 0, 0, 0, 0, pi, 0)
    rotateRobot(move_group, 0, 0, 0, 0, pi, pi/12)
    rotateRobot(move_group, 0, 0, -pi/12, 0, pi, pi/12)
    ee_group.go([0.1])
    #print("Robot is ready to move chips.")
    attach_detach_helper.attach_chip(chip_name)
    time.sleep(2)
    rotateRobot(move_group, 0, 0, 0, 0, pi, pi/12)
    time.sleep(20)
    rotateRobot(move_group, pi/8, 0, 0, 0, pi, pi/12)
    rotateRobot(move_group, pi/4, 0, 0, 0, pi, pi/12)
    attach_detach_helper.detach_chip(chip_name)
    ee_group.go([0])
    

    


def main():

    # All the code for this set up section is just from the tutorial, nothing really to comment on
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
    robot = moveit_commander.RobotCommander() # use to get current info 
    scene = moveit_commander.PlanningSceneInterface()
    group_name = "ur5e_arm" # ADDED FROM JONATHAN
    move_group = moveit_commander.MoveGroupCommander(group_name)
    ee_group = moveit_commander.MoveGroupCommander("robotiq_hand") #ADDED FROM JONATHAN
    attach_detach_helper = AttachDetachHelper()
    object_controller = ObjectController()

    display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
    )
    planning_frame = move_group.get_planning_frame()
    eef_link = move_group.get_end_effector_link()
    group_names = robot.get_group_names()



    robotToSafePosition(move_group)
    ee_group.go([0])
    robotToBJStartPoint(move_group)

    # Set up the Player's chips
    player_chips = Chips()  # remember the default value is 100

    while True:

        spawnOurChip()
        time.sleep(3)
        print('Welcome to BlackJack! Get as close to 21 as you can without going over!\n\
        Dealer hits until she reaches 17. Aces count as 1 or 11.')
        print("\nPlayer's pot stand at",player_chips.total)
        # Create & shuffle the deck, deal two cards to each player
        deck = Deck()
        deck.shuffle()
        player_hand = Hand()
        player_hand.add_card(deck.deal())
        player_hand.add_card(deck.deal())
        dealer_hand = Hand()
        dealer_hand.add_card(deck.deal())
        dealer_hand.add_card(deck.deal())
        
        # Prompt the Player for their bet
        take_bet(player_chips)

        
        chip_name = "chip_link"
        robotBetsChips(move_group, ee_group, attach_detach_helper, chip_name)
        

        robotToSafePosition(move_group)
        robotToBJStartPoint(move_group)

        # Show cards (but keep one dealer card hidden)
        show_some(player_hand,dealer_hand)

        playing = True

        while playing:  # recall this variable from our hit_or_stand function

            # Prompt for Player to Hit or Stand
            playing = hit_or_stand(deck,player_hand, move_group)

            # Show cards (but keep one dealer card hidden)
            show_some(player_hand,dealer_hand)

            # If player's hand exceeds 21, run player_busts() and break out of loop
            if player_hand.value > 21:
                player_busts(player_hand,dealer_hand,player_chips)
                break


                # If Player hasn't busted, play Dealer's hand until Dealer reaches 17
        if player_hand.value <= 21:

            while dealer_hand.value < 17:
                hit(deck,dealer_hand)
                time.sleep(2)

                # Show all cards
            show_all(player_hand,dealer_hand)

            # Run different winning scenarios
            if dealer_hand.value > 21:
                dealer_busts(player_hand,dealer_hand,player_chips)

            elif dealer_hand.value > player_hand.value:
                dealer_wins(player_hand,dealer_hand,player_chips)

            elif dealer_hand.value < player_hand.value:
                player_wins(player_hand,dealer_hand,player_chips)

            else:
                push(player_hand,dealer_hand)

                # Inform Player of their chips total
        print("\nPlayer's pot stand at",player_chips.total)

        deleteOurChip()
        time.sleep(2)
        
        # Ask to play again
        new_game = input("Would you like to play another hand? Enter 'y' or 'n' ")

        if new_game[0].lower()=='y':
            playing=True
            continue
        else:
            print("Thanks for playing!")
            break
        

if __name__ == "__main__":
    main()
