import rospy
#import gazebo_ros_link_attacher
#import Attach
from gazebo_ros_link_attacher.srv import Attach

class AttachDetachHelper():
    def __init__(self):
        self._attach_service = rospy.ServiceProxy("/link_attacher_node/attach", Attach)
        self._detach_service = rospy.ServiceProxy("/link_attacher_node/detach", Attach)

    def attach_chip(self, chip_name: str):
        return self._attach_service(
            "robot",
            "wrist_3_link",
            chip_name,
            "chip_link"
        )

    def detach_chip(self, chip_name: str):
        return self._detach_service(
            "robot",
            "wrist_3_link",
            chip_name,
            "chip_link"
        )

    """
     def link_board_to_chip(self, chip_name: str):
        return self._attach_service(
            "robot",
            "board_link",
            chip_name,
            "chip_link"
        )
    """
   